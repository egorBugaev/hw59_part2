import React from 'react';
import JokeList from "./Jokes";

const Jokes = props => {
  return (
    <div className="items-list">
       <button onClick={props.newJoke}>New jokes</button>

      {props.joke.map(joke => (
        <JokeList key={joke.id} joke={joke.value}/>
      ))}
    </div>
  )
};

export default Jokes;