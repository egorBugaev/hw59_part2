import React, { Component } from 'react';
import './App.css';
import Jokes from "./Joke";

class App extends Component {
  state = {
    jokes:[]
  };


  newJoke = () => {
   const updatedJokes = [];
   for (let i = 0; i<5;i++) {
     const URL = 'https://api.chucknorris.io/jokes/random';
     fetch(URL).then(response => {
       if (response.ok) {
         return response.json();
       }
       throw new Error('Something went wrong with network request');
     }).then(resp => {

       updatedJokes.push(resp);

       this.setState({jokes: updatedJokes});
       console.log(updatedJokes);
     }).catch(error => {
       console.log(error);
     });
   }
 };
  componentDidMount() {
    this.newJoke()
    }


  render() {
    return (
      <div className="App">
       <Jokes
        joke={this.state.jokes}
        newJoke={this.newJoke}
       />

      </div>
    );
  }
}

export default App;
